package PgCommitFest::PatchComment;
use strict;
use warnings;

sub controls {
	my ($r, $d) = @_;
	$r->add_control('patch_comment_type', 'select', 'Comment Type',
		'required' => 1);
	$r->control('patch_comment_type')->choice($r->db->select(<<EOM));
SELECT id, name FROM patch_comment_type ORDER BY id
EOM
	$r->add_control('message_id', 'text', 'Message-ID', 'maxlength' => 255);
	$r->add_control('content', 'textarea', 'Content', 'required' => 1);
	return $r->initialize_controls($d);
}

sub delete {
	my ($r) = @_;
	my $aa = $r->authenticate('require_login' => 1);
	$r->set_title('Delete Patch Comment');
	my $d;
	eval {
		my $id = $r->cgi_required_id();
		if (! $aa->{'is_administrator'}) {
			$d = $r->db->select_one(<<EOM, $id);
SELECT creator FROM patch_comment WHERE id = ?
EOM
			if ($aa->{'userid'} ne $d->{'creator'})  {
				$r->error_exit(<<EOM);
Only administrators can delete comments created by other users.
EOM
			}
		}
		# Don't bump last_updated_time, as that would trigger an activity log
		# record.  But do change the last_updater, so that the subsequent
		# delete picks up the correct user id.  This is a pretty ugly kludge,
		# but I don't immediately have a better idea.
		$r->db->update('patch_comment', { 'id' => $id },
			{ 'last_updater' => $aa->{'userid'} });
		$d = $r->db->select_one(<<EOM, $id);
DELETE FROM patch_comment WHERE id = ? RETURNING patch_id
EOM
	};
	my $err = $@;
	if (! $err) {
		$r->error_exit('Patch not found.') if !defined $d;
		$r->db->commit;
		$r->redirect('/action/patch_view?id=' . $d->{'patch_id'});
	}
	$r->error_exit("Internal error: $@");
}

sub form {
	my ($r) = @_;
	my $aa = $r->authenticate('require_login' => 1);

	# Decide whether this is a new comment or an edit of an existing
	# comment, and if editing reload data from database.
	my $d;
	my $id = $r->cgi_id();
	if (defined $id) {
		$d = $r->db->select_one(<<EOM, $id);
SELECT id, patch_id, patch_name, patch_comment_type_id AS patch_comment_type,
	message_id, content, creator FROM patch_comment_view WHERE id = ?
EOM
		$r->error_exit('Patch comment not found.') if !defined $d;
		if (! $aa->{'is_administrator'}
			&& $aa->{'userid'} ne $d->{'creator'})  {
			$r->error_exit(<<EOM);
Only administrators can edit comments created by other users.
EOM
		}
		$r->set_title('Edit Patch Comment: ' . $d->{'patch_name'});
	}
	else {
		$d = $r->db->select_one(<<EOM, $r->cgi_required_id('patch'));
SELECT id AS patch_id, name AS patch_name FROM patch WHERE id = ?
EOM
		$r->error_exit('Patch not found.') if !defined $d;
		$r->set_title('New Patch Comment: ' . $d->{'patch_name'});
	}
	$r->redirect('/action/patch_view?id=' . $d->{'patch_id'})
		if $r->cgi('cancel');

	# Add controls.
	my %value = controls($r, $d);

	# Handle commit.
	if ($r->cgi('go') && ! $r->is_error()) {
		$value{'last_updated_time'} = \'now()';
		$value{'last_updater'} = $aa->{'userid'};
		if (defined $id) {
			$r->db->update('patch_comment', { 'id' => $id }, \%value);
		}
		else {
			$value{'patch_id'} = $d->{'patch_id'};
			$value{'creator'} = $aa->{'userid'};
			$id = $r->db->insert_returning_id('patch_comment', \%value);
		}
		$r->db->commit;
		$r->redirect('/action/patch_view?id=' . $d->{'patch_id'});
	}

	# Display template.
	$r->render_template('patch_comment_form', { 'id' => $id, 'd' => $d });
}

1;
