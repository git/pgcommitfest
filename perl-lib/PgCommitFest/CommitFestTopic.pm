package PgCommitFest::CommitFestTopic;
use strict;
use warnings;

sub delete {
	my ($r) = @_;
	$r->authenticate('require_login' => 1);
	$r->set_title('Delete CommitFest Topic');
	my $d;
	eval {
		$d = $r->db->select_one(<<EOM, $r->cgi_required_id);
DELETE FROM commitfest_topic WHERE id = ? RETURNING commitfest_id
EOM
	};
	my $err = $@;
	if (! $err) {
		$r->error_exit('CommitFest not found.') if !defined $d;
		$r->db->commit;
		$r->redirect('/action/commitfest_topic_search?id='
			. $d->{'commitfest_id'});
	}
	if ($err =~ /patch_commitfest_topic_id_fkey/) {
		$r->error_exit(<<EOM);
This CommitFest topic contains one or more patches and can't be deleted.
EOM
	}
	$r->error_exit("Internal error: $@");
}

sub form {
	my ($r) = @_;
	$r->authenticate('require_login' => 1);

	# Decide whether this is a new commitfest or an edit of an existing
	# commitfest, and if editing reload data from database.
	my $d;
	my $id = $r->cgi_id();
	if (defined $id) {
		$r->set_title('Edit CommitFest Topic');
		$d = $r->db->select_one(<<EOM, $id);
SELECT id, commitfest_id, name, sortorder FROM commitfest_topic WHERE id = ?
EOM
		$r->error_exit('CommitFest not found.') if !defined $d;
	}
	else {
		$d = $r->db->select_one(<<EOM, $r->cgi_required_id('commitfest'));
SELECT id AS commitfest_id, 50 AS sortorder FROM commitfest WHERE id = ?
EOM
		$r->set_title('New CommitFest Topic');
	}
	$r->redirect('/action/commitfest_topic_search?id=' . $d->{'commitfest_id'})
		if $r->cgi('cancel');

	# Add controls.
	$r->add_control('name', 'text', 'Name', 'required' => 1);
	$r->add_control('sortorder', 'integer', 'Sort Order', 'required' => 1,
		'min_value' => 0);
	my %value = $r->initialize_controls($d);

	# Handle commit.
	if ($r->cgi('go') && ! $r->is_error()) {
		if (defined $id) {
			$r->db->update('commitfest_topic', { 'id' => $id }, \%value);
		}
		else {
			$id = $r->db->insert_returning_id('commitfest_topic', {
				'commitfest_id' => $d->{'commitfest_id'},
				%value
			});
		}
		$r->db->commit;
		$r->redirect('/action/commitfest_topic_search?id='
			. $d->{'commitfest_id'});
	}

	# Display template.
	$r->render_template('commitfest_topic_form', { 'id' => $id, 'd' => $d });
}

sub search {
	my ($r) = @_;
	my $id = $r->cgi_id();
	my $d = $r->db->select_one(<<EOM, $id) if defined $id;
SELECT id, name FROM commitfest_view WHERE id = ?
EOM
	$r->error_exit('CommitFest not found.') if !defined $d;
	$r->set_title('CommitFest Topics: %s', $d->{'name'});

	my $topic_list = $r->db->select(<<EOM, $d->{'id'});
SELECT id, name, sortorder FROM commitfest_topic
	WHERE commitfest_id = ? ORDER BY sortorder, name
EOM

	$r->add_link('/action/commitfest_topic_form?commitfest=' . $id,
		'New Topic');
	$r->add_link('/action/commitfest_view?id=' . $id, 'Back to CommitFest');
	$r->render_template('commitfest_topic_search',
		{ 'topic_list' => $topic_list });
}

1;
