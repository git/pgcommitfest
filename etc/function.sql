CREATE LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION most_recent_comments(integer)
	RETURNS SETOF patch_comment_view AS $$
DECLARE
	v_patch_id integer;
BEGIN
	FOR v_patch_id IN
		SELECT
			p.id
		FROM
			patch p
			INNER JOIN commitfest_topic t ON p.commitfest_topic_id = t.id
		WHERE
			t.commitfest_id = $1
	LOOP
		RETURN QUERY (
			SELECT * FROM patch_comment_view WHERE patch_id = v_patch_id
				ORDER BY creation_time DESC LIMIT 3
		);
	END LOOP;
END
$$ LANGUAGE plpgsql;

-- Dummy implementation for testing purposes.
CREATE OR REPLACE FUNCTION community_login(INOUT userid text, password text,
	OUT success integer) RETURNS record AS $$
BEGIN
	SELECT userid, CASE WHEN LOWER(userid) = 'rhaas' AND password = 'bob'
		THEN 1 ELSE 0 END
	INTO userid, success;
END
$$ LANGUAGE plpgsql;
